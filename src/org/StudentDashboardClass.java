package org;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import org.data.Lesson;
import org.data.ReviewLesson3;
import org.data.Student;


public class StudentDashboardClass {

	Student student2=new Student();
	
	public void init3() {
		login();
	}
	
	public void login() {
		Scanner sc=new Scanner(System.in);
		boolean isContinue=true;
		while (isContinue) {
			try {
				
				System.out.println("Enter students ID : (Type 'home' for goto home) ");
				String stdId2=sc.nextLine();
				switch (stdId2) {
				case "home":
					isContinue=false;
					break;

				default:
					if(student2.isStudentRegistered3(stdId2)) {
						System.out.println("correct student id ");
						showDashboard();
					}
					else {
						
							System.out.println("please enter a valid Student id !");
						
					}
					break;
				}
				
				
				
			}catch (InputMismatchException e) {
				System.out.println("invalid input please enter correct one ");
				sc=new Scanner(System.in);
			}
			catch (Exception e) {
				System.out.println("invalid input please enter correct one ");
				sc=new Scanner(System.in);
			}
		}
		
	}
	
	public void showDashboard() {
		
		Scanner sc=new Scanner(System.in);
		boolean isContinue=true;
		while (isContinue) {
			
			try {
				System.out.println("Welcome "+DataGenerator.logedInStudent3.getStdName3());
				System.out.println(" Choose from the following options press.. \n"
						+ " 1. View Timetable and Book Lesson\n"
						+ " 2. Change Booking\n"
						+ " 3. Submit Review\n"
						+ " 4. Log out ");
				System.out.println("Enter your choise... ");
				
				String opt=sc.nextLine();
				
				switch (opt) {
				case "1":
					SearchTimeTable();
					break;
				case "2":
					initRemove();
					break;
				case "3":
					initReview();
					
					break;
				case "4":
					isContinue=false;
					break;
				default:
					
					break;
				}
			} catch (Exception e) {
				System.out.println("invalid choise");
				sc=new Scanner(System.in);
			}
			
			
		}
		
		
	}
	
	public void SearchTimeTable() {
		List<Lesson> lessonsList2=new ArrayList<Lesson>();
		Scanner sc=new Scanner(System.in);
		boolean isContinue=true;
		while (isContinue) {
		
			try {
				System.out.println(" Choose from the following options press.. \n"
						+ " 1. Search by day\n"
						+ " 2. Search by exercise\n"
						+ " 3. Back");
				System.out.println("Enter your choise... ");
				String opt=sc.nextLine();
				
				switch (opt) {
				case "1":
					System.out.println("Enter day (Saturday/Sunday)");
					String inputDay=sc.nextLine();
					if(inputDay.equalsIgnoreCase("saturday")|| inputDay.equalsIgnoreCase("sunday")) {
						lessonsList2=searchByDay(inputDay);
						if(lessonsList2.size()>0) {
							displayTimeTable(lessonsList2);
						}
						else {
							System.out.println("no lessons found try another day");
						}
						
					}
					else {
						System.out.println("enter a valid day try again");
					}
					break;
				case "2":
					System.out.println("Enter exercise name [Aquacise , Body Blitz , Box Fit , Yoga] ");
					String inputExercise=sc.nextLine();
					lessonsList2=searchByExercise(inputExercise);
					if(lessonsList2.size()>0) {
						displayTimeTable(lessonsList2);
					}
					else {
						System.out.println("no lessons found try another exercise");
					}
					break;
				case "3":
					isContinue=false;
					break;

				default:
					System.out.println("inavlid choise");
					break;
				}
				
			} catch (Exception e) {
				System.out.println("exception occured");
			}
			
		}
		
	}
	
	public List<Lesson> searchByDay(String inputDay) {
		List<Lesson> lessonsList2=new ArrayList<Lesson>();
		
			
			for (Lesson lesson2 : DataGenerator.upcommingLessons3) {
				String day=lesson2.getDate3().split("\\s+")[0].trim();
				if(day.equalsIgnoreCase(inputDay)) {
					lessonsList2.add(lesson2);
				}
			}
		return lessonsList2;
		
	}
	public List<Lesson> searchByExercise(String inputExercise) {
		List<Lesson> lessonsList2=new ArrayList<Lesson>();
		
		for (Lesson lesson2 : DataGenerator.upcommingLessons3) {
			
			if(lesson2.getExercise3().getexerciseName3().equalsIgnoreCase(inputExercise)) {
				lessonsList2.add(lesson2);
			}
				
		}
		
			
		return lessonsList2;
		
	}
	
	public void displayTimeTable(List<Lesson> lessonsList2) {
		
		
		Scanner sc=new Scanner(System.in);
		try {
			System.out.printf("%-6s | %-13s | %-6s | %-25s | %-10s |%s%n", "ID","EXERCISE","PRICE","DATE","TIMMING","AVAILABLE SEATS");
			System.out.println("-----------------------------------------------------------------------------------------------------");
			//	System.out.println(" ID  EXERCISE  PRICE  DATE  TIMMING  AVAILABLE SEATS  ");
				for (Lesson lesson2 : lessonsList2) {
					System.out.printf("%-6s | %-13s | %-6d | %-25s | %-10s |%s%n",
							lesson2.getId3(),lesson2.getExercise3().getexerciseName3(),
							lesson2.getExercise3().getPrice3(),lesson2.getDate3(),
							lesson2.getTiming3(),(4-lesson2.getStudents3()));
//					System.out.println(lesson2.getId3()+lesson2.getExercise3().getexerciseName3()
//							+lesson2.getExercise3().getPrice3()
//							+lesson2.getDate3()+lesson2.getTiming3()
//							+(4-lesson2.getStudents3()) );		
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
			
			boolean isContinue=true;
			while (isContinue) {
				try {
					System.out.println("Do you want to book a new lesson from above list [yes/no] : ");
					String opt=sc.nextLine();
					
					switch (opt) {
					case "yes":
						initBooking();
						
						break;
					case "no":
						isContinue=false;
						break;

					default:
						System.out.println("enter a valid choise");
						break;
					}
				} catch (Exception e) {
					System.out.println("enter a valid choise ");
					sc=new Scanner(System.in);
				}
			}
			
	}
	
	public void initBooking() {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter lesson ID : ");
		try {
			Lesson lesson2=new Lesson();
			
			long inputId=Long.parseLong(sc.nextLine());
			boolean isLessonExist=lesson2.isLessonExist(inputId);
			
			if(isLessonExist) {
				bookLesson(inputId);
			}
			else {
				System.out.println("plesae enter a valid ID : ");
			}
		} catch (Exception e) {
			System.out.println("plesae enter a valid ID : ");
			sc=new Scanner(System.in);
		}
		
	}
	
	
	public void bookLesson(long inputId) {
		for (Lesson lesson2 : DataGenerator.upcommingLessons3) {
			
			if(inputId==lesson2.getId3()) {
				if(lesson2.isSeatVacant()) {
					
					boolean flag2=true;
					for(long stdLessonId2:DataGenerator.logedInStudent3.getBookedLessonsId3()) {
					
						Lesson stdLesson2=new Lesson();
						for(Lesson tempLesson2:DataGenerator.upcommingLessons3) {
							if(tempLesson2.getId3()==stdLessonId2) {
								stdLesson2=tempLesson2;
								break;
							}
						}

						if(lesson2.getId3()==stdLessonId2) {
							 System.out.println(" You already booked this lesson !");
							 flag2=false;
						}
						else if(lesson2.getDate3().equalsIgnoreCase(stdLesson2.getDate3()) && lesson2.getTiming3().equalsIgnoreCase(stdLesson2.getTiming3())) {
							System.out.println(" Time clashed with already booked lesson !");  
							flag2=false;
						}
						
					}
					if(flag2) {
						DataGenerator.logedInStudent3.addBookedLessonsId3(lesson2.getId3());
						lesson2.setStudents3(lesson2.getStudents3()+1);
						System.out.println(" Lesson booked successfully !"); 
						
					}
					
				}
				else {
					 System.out.println("All seats are full , No Seat Available !");  
				}
				break;
			}
			
		}
		
	}
	
	public void initReview() {
		showAttendedLessons();
		Scanner sc=new Scanner(System.in);
		boolean isContinue=true;
		while (isContinue) {
			try {
				System.out.println("Enter lesson ID : (type 'back' for go back)");
				String input=sc.nextLine();
				switch (input) {
				case "back":
					isContinue=false;
					break;
				default:
					try {
						long inputId=Long.parseLong(input);
						
						
						if(isAttended(inputId)) {
							submitReview(inputId);
						}
						else {
							System.out.println("enter a valid lesson ID ");
						}
					} catch (Exception e) {
						System.out.println("invalid input ");
						sc=new Scanner(System.in);
					}
					break;
				}
				
			} catch (Exception e) {
				System.out.println("invalid input");
				sc=new Scanner(System.in);
			}
		}
		
	}
	
	public void submitReview(long lessonId) {
		
		Scanner sc=new Scanner(System.in);
		boolean isContinue=true;
		String ratings2[]= {"1: Very dissatisfied","2: Dissatisfied","3: Ok","4: Satisfied","5: Very Satisfied"};
		while (isContinue) {
			System.out.println("enter rating ");
			for (String rating : ratings2) {
				System.out.println(rating);
			}
			try {
				int rating=Integer.parseInt(sc.nextLine()); 
				if(rating>0 && rating<6 ) {
					System.out.println(" write review ");
					String review=sc.nextLine();
					DataGenerator.lessonReviewList3.add(
							new ReviewLesson3(lessonId, 
									DataGenerator.logedInStudent3.getStdId3(), 
									review, rating));
					System.out.println("Review Updated  !");
					isContinue=false;
				}
				else {
					System.out.println("enter a valid rating");
				}
			} catch (Exception e) {
				System.out.println("enter valid input");
				sc=new Scanner(System.in);
			}
			
		}
		
	
	}
	
	public void showAttendedLessons() {

		List<Lesson> lessons2=getAttendedLessonsList2();
	
		try {
			System.out.printf("%-6s | %-13s | %-6s | %-25s | %-10s |%n", "ID","EXERCISE","PRICE","DATE","TIMMING");
			System.out.println("-----------------------------------------------------------------------------------------------------");
	
		//	System.out.println(" ID  EXERCISE  PRICE  DATE  TIMMING ");
			
			for (Lesson lesson2 : lessons2) {
				System.out.printf("%-6s | %-13s | %-6s | %-25s | %-10s |%n", 
						lesson2.getId3(),
						lesson2.getExercise3().getexerciseName3(),
						lesson2.getExercise3().getPrice3(),
						lesson2.getDate3() ,
						lesson2.getTiming3());

//				System.out.println(lesson2.getId3()
//						+lesson2.getExercise3().getexerciseName3()
//						+lesson2.getExercise3().getPrice3()
//						+lesson2.getDate3() 
//						+lesson2.getTiming3() );
	
			}

		} catch (Exception ex2) {
			ex2.printStackTrace();
		}

	}
	
	private List<Lesson> getAttendedLessonsList2() {
		List<Lesson> attendedLessons2=new ArrayList<Lesson>();
		for (long stdLessonId2 : DataGenerator.logedInStudent3.getAttendedLessonsId3()) {
			
			for(Lesson lesson2:DataGenerator.attantedLessons3) {
				if(lesson2.getId3()==stdLessonId2) {
					attendedLessons2.add(lesson2);
				}
			}
			
		}
		return attendedLessons2;
	}
	
	private boolean isAttended(long inputId) {
		boolean flag=false;
		for (long stdLessonId2 : DataGenerator.logedInStudent3.getAttendedLessonsId3()) {
			
			
				if(inputId==stdLessonId2) {
					flag=true;
				}
			
			
		}
		return flag;
	}
	
	public void initRemove() {
		showBookings();
		Scanner sc=new Scanner(System.in);
		boolean isContinue=true;
		while (isContinue) {
			try {
				System.out.println("Enter lesson ID : (type 'back' for go back)");
				String input=sc.nextLine();
				switch (input) {
				case "back":
					isContinue=false;
					break;

				default:
					try {
						long inputId=Long.parseLong(input);
						
						if(isBooked(inputId)) {
							removeLesson(inputId);
							System.out.println("Redirecting to search and book new lesson");
							SearchTimeTable();
							isContinue=false;
						}
						else {
							System.out.println("enter a valid lesson ID ");
						}
					} catch (Exception e) {
						System.out.println("invalid input ");
						sc=new Scanner(System.in);
					}
					break;
				}
			} catch (Exception e) {
				System.out.println("invalid input ");
				sc=new Scanner(System.in);
			}
		}
			
	}
	
	public void removeLesson(long inputId) {
		DataGenerator.logedInStudent3.getBookedLessonsId3().remove(inputId);
		
		for(Lesson lesson:DataGenerator.upcommingLessons3) {
			if(lesson.getId3()==inputId) {
		
				lesson.setStudents3(lesson.getStudents3()-1);
				System.out.println(" Lesson removed successfully !"); 
				
				break;
			}
		}
	}
	private void showBookings() {

		List<Lesson> lessonsList2=getBookedLessons2();
	
		try {
			System.out.printf("%-6s | %-13s | %-6s | %-25s | %-10s |%n", "ID","EXERCISE","PRICE","DATE","TIMMING");
			System.out.println("-----------------------------------------------------------------------------------------------------");
	
			//System.out.println(" ID  EXERCISE  PRICE  DATE  TIMMING ");
			for (Lesson lesson2 : lessonsList2) {
				System.out.printf("%-6s | %-13s | %-6s | %-25s | %-10s |%n", 
						lesson2.getId3(),
						lesson2.getExercise3().getexerciseName3(),
						lesson2.getExercise3().getPrice3(),
						lesson2.getDate3() ,
						lesson2.getTiming3() );

//				System.out.println(lesson2.getId3()
//						+lesson2.getExercise3().getexerciseName3()
//						+lesson2.getExercise3().getPrice3() 
//						+lesson2.getDate3() 
//						+lesson2.getTiming3() );
				
			}


		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
	
	public List<Lesson> getBookedLessons2() {
		List<Lesson> bookedLessons2=new ArrayList<Lesson>();
		for (long stdLessonId2 : DataGenerator.logedInStudent3.getBookedLessonsId3()) {
			
			for(Lesson lesson2:DataGenerator.upcommingLessons3) {
				if(lesson2.getId3()==stdLessonId2) {
					bookedLessons2.add(lesson2);
				}
			}
			
		}
		return bookedLessons2;
	}
	
	public boolean isBooked(long inputId) {
		boolean flag=false;
		for (long stdLessonId2 : DataGenerator.logedInStudent3.getBookedLessonsId3()) {
			
			
				if(inputId==stdLessonId2) {
					flag=true;
				}
			
			
		}
		return flag;
	}
	
	
}
