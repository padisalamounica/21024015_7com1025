package org.data;

import org.DataGenerator;

public class Lesson  {
	
	private static long cont3=1;

	private long lessonId3=0;
	private GroupExercise3 exercise3;
	String timing3;
	String date3;
	int students3;
	
	int stdAttend3=0;
	int totalRating3=0;
	
	public Lesson() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Lesson(GroupExercise3 exercise3, String timing3,  String week3, int students3) {
		
		this.exercise3 = exercise3;
		this.timing3 = timing3;
		this.date3 = week3;
		this.students3 = students3;
		
		this.lessonId3=cont3++;
	}
	
	public Lesson(long lessonId3,GroupExercise3 exercise3, String timing3, String week3, int students3) {
		
		this.lessonId3=lessonId3;
		this.exercise3 = exercise3;
		this.timing3 = timing3;
		this.date3 = week3;
		this.students3 = students3;
		
	}
	public GroupExercise3 getExercise3() {
		return exercise3;
	}
	public void setExercise3(GroupExercise3 exercise3) {
		this.exercise3 = exercise3;
	}
	public String getTiming3() {
		return timing3;
	}
	public void setTiming3(String timing3) {
		this.timing3 = timing3;
	}

	public String getDate3() {
		return date3;
	}
	public void setDate3(String date3) {
		this.date3 = date3;
	}
	public int getStudents3() {
		return students3;
	}
	public void setStudents3(int students3) {
		this.students3 = students3;
	}

	public int getStdAttend3() {
		return stdAttend3;
	}

	public void setStdAttend3(int stdAttend3) {
		this.stdAttend3 = stdAttend3;
	}

	

	public int getTotalRating3() {
		return totalRating3;
	}

	public void setTotalRating3(int totalRating3) {
		this.totalRating3 = this.totalRating3+totalRating3;
	}

	public long getId3() {
		return lessonId3;
	}


	public boolean isSeatVacant() {
		
		if(getStudents3()<4) {
			return true;
		}
		else {
			return false;
		}
		
	}
	
public boolean isLessonExist(long inputId) {
	boolean flag=false;
	for (Lesson lesson2 : DataGenerator.upcommingLessons3) {
		if(inputId==lesson2.getId3()) {
			flag=true;
			break;
		}
	}
	return flag;
}
	
}
