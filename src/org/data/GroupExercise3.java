package org.data;

public class GroupExercise3 {
	
	String exerciseName3;
	long price3;
	public GroupExercise3(String name3, long price3) {
		super();
		this.exerciseName3 = name3;
		this.price3 = price3;
	}
	public String getexerciseName3() {
		return exerciseName3;
	}
	public void setexerciseName3(String name3) {
		this.exerciseName3 = name3;
	}
	public long getPrice3() {
		return price3;
	}
	public void setPrice3(long price3) {
		this.price3 = price3;
	}
	
}
