package org.data;

public class ReviewLesson3 {

	long lessonId3;
	String stdId3;
	String reviewText3;
	int rating3;
	public ReviewLesson3(long lessonId3, String stdId3, String reviewText3, int rating3) {
		super();
		this.lessonId3 = lessonId3;
		this.stdId3 = stdId3;
		this.reviewText3 = reviewText3;
		this.rating3 = rating3;
	}
	public long getLessonId3() {
		return lessonId3;
	}
	public void setLessonId3(long lessonId3) {
		this.lessonId3 = lessonId3;
	}
	public String getStdId3() {
		return stdId3;
	}
	public void setStdId3(String stdId3) {
		this.stdId3 = stdId3;
	}
	public String getReviewText3() {
		return reviewText3;
	}
	public void setReviewText3(String reviewText3) {
		this.reviewText3 = reviewText3;
	}
	public int getRating3() {
		return rating3;
	}
	public void setRating3(int rating3) {
		this.rating3 = rating3;
	}
	
	
	
	@Override
	public int hashCode() {
		final int prime3 = 31;
		int result3 = 1;
		result3 = prime3 * result3 + (int) (lessonId3 ^ (lessonId3 >>> 32));
		result3 = prime3 * result3 + ((stdId3 == null) ? 0 : stdId3.hashCode());
		return result3;
	}
	@Override
	public boolean equals(Object obj3) {
		if (this == obj3)
			return true;
		if (obj3 == null)
			return false;
		if (getClass() != obj3.getClass())
			return false;
		ReviewLesson3 other3 = (ReviewLesson3) obj3;
		if (lessonId3 != other3.lessonId3)
			return false;
		if (stdId3 == null) {
			if (other3.stdId3 != null)
				return false;
		} else if (!stdId3.equals(other3.stdId3))
			return false;
		return true;
	}
	
	
}
