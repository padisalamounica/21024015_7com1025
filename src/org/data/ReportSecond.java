package org.data;

public class ReportSecond {
	
	private String exerciseName3;
	private long lessons3=0;
	
	private double totalIncome3=0;

	public ReportSecond(String exerciseName3, long lessons3, double totalIncome3) {
		super();
		this.exerciseName3 = exerciseName3;
		this.lessons3 = this.lessons3+lessons3;
		this.totalIncome3 = this.totalIncome3+totalIncome3;
	}

	public String getExerciseName3() {
		return exerciseName3;
	}

	public void setExerciseName3(String exerciseName3) {
		this.exerciseName3 = exerciseName3;
	}

	public long getLessons3() {
		return lessons3;
	}

	public void setLessons3(long lessons3) {
		this.lessons3 = this.lessons3+lessons3;
	}

	public double getTotalIncome3() {
		return totalIncome3;
	}

	public void setTotalIncome3(double totalIncome3) {
		this.totalIncome3 = this.totalIncome3 +totalIncome3;
	}

}
