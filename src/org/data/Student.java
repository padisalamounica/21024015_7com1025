package org.data;

import java.util.ArrayList;
import java.util.List;

import org.DataGenerator;
import org.Application;


public class Student {

	
		public static List<Student> REGISTERED_STUDENTSList3= new ArrayList<Student>();
	
	private String stdId3;
	private String stdName3;
	public List<Long> bookedLessonsId3=new ArrayList<Long>();
	public List<Long> attendedLessonsId3=new ArrayList<Long>();
	
	
	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Student(String id3, String name3) {
		super();
		this.stdId3 = id3;
		this.stdName3 = name3;
	}
	public String getStdId3() {
		return stdId3;
	}
	public void setStdId3(String id3) {
		this.stdId3 = id3;
	}
	public String getStdName3() {
		return stdName3;
	}
	public void setStdName3(String stdName3) {
		this.stdName3 = stdName3;
	}
	public List<Long> getBookedLessonsId3() {
		return bookedLessonsId3;
	}
	public void addBookedLessonsId3(long lessonsId3) {
		this.bookedLessonsId3.add(lessonsId3);
	}
	
	public List<Long> getAttendedLessonsId3() {
		return attendedLessonsId3;
	}
	public void addAttendedLessonsId3(long attendedLessonsId3) {
		this.attendedLessonsId3.add(attendedLessonsId3);
	}
	
	public boolean isStudentRegistered3(String stdId3) {
		boolean isStudentExist3=false;
		for (Student student3 : Student.REGISTERED_STUDENTSList3) {
			
			if(student3.getStdId3().equalsIgnoreCase(stdId3)) {
				
				DataGenerator.logedInStudent3=student3;
				isStudentExist3=true;
				return isStudentExist3;
				
			}
		}
		return isStudentExist3;
	}
	
	
}
