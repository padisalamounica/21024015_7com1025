package org;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.data.GroupExercise3;
import org.data.Lesson;
import org.data.ReviewLesson3;
import org.data.Student;

public class DataGenerator {
	public static List<Lesson> upcommingLessons3=new ArrayList<Lesson>();
	public static List<Lesson> attantedLessons3=new ArrayList<Lesson>();
	public static Set<ReviewLesson3> lessonReviewList3=new HashSet<ReviewLesson3>();
	public static Student logedInStudent3;
	public static int index3=0;
	
	public  List<String> generateUpcommingDates(Date upcomingStartDate) {
		
		List<String> dateList3=new ArrayList<String>();
		Calendar cal3 = Calendar.getInstance();
		cal3.setTime(upcomingStartDate);
		int dayOfWeek3 = 0;
		
		SimpleDateFormat dateFormate3 = new SimpleDateFormat("EEEEE dd MMMMM yyyy");		
		String formattedDate3 = dateFormate3.format(cal3.getTime());
		
		
		for(int i=0;i<9;i++) {
			
			dayOfWeek3 = cal3.get(Calendar.DAY_OF_WEEK);
			cal3.add(Calendar.DATE, 7-dayOfWeek3);
			formattedDate3 = dateFormate3.format(cal3.getTime());
			dateList3.add(formattedDate3);
			
			dayOfWeek3 = cal3.get(Calendar.DAY_OF_WEEK);
			cal3.add(Calendar.DATE, 1);
			formattedDate3 = dateFormate3.format(cal3.getTime());
			dateList3.add(formattedDate3);
			
			dayOfWeek3 = cal3.get(Calendar.DAY_OF_WEEK);
			cal3.add(Calendar.DATE, 1);
			
			
		}
		
		return dateList3;
	}
	
public  List<String> generatePreviousDates(Date previousStartDate) {
		
		List<String> dateList3=new ArrayList<String>();
		Calendar cal3 = Calendar.getInstance();
		cal3.setTime(previousStartDate);
		int dayOfWeek3 = 0;
		
		SimpleDateFormat dateFormate3 = new SimpleDateFormat("EEEEE dd MMMMM yyyy");		
		String formattedDate3 = dateFormate3.format(cal3.getTime());
		
		
		for(int i=0;i<10;i++) {
			
			 dayOfWeek3 = cal3.get(Calendar.DAY_OF_WEEK);
			cal3.add(Calendar.DATE, 1-dayOfWeek3);
			formattedDate3 = dateFormate3.format(cal3.getTime());
			dateList3.add(formattedDate3);
			
			dayOfWeek3 = cal3.get(Calendar.DAY_OF_WEEK);
			cal3.add(Calendar.DATE, -1);
			formattedDate3 = dateFormate3.format(cal3.getTime());
			dateList3.add(formattedDate3);
			
			dayOfWeek3 = cal3.get(Calendar.DAY_OF_WEEK);
			cal3.add(Calendar.DATE, -1);
			
			
		}
		
		return dateList3;
	}

	
	
	public int getRandomNumber3(int min3,int max3) {
	  
		
		int num3 = (int)(Math.random()*(max3-min3+1)+min3);  
		  
		
		return num3;
	}
	
	public int getIndex() {
	  
		int num3=0;
		if(index3>3) {
			index3= 0;
			num3=index3;
		}
		else {
			num3= index3;
		}
		 
		  
		index3++;
		return num3;
	}
	
	
	public void generateStudentsList() {
		
		
		Student.REGISTERED_STUDENTSList3.add(new Student("CS10","Rueben Hutton" ));
		Student.REGISTERED_STUDENTSList3.add(new Student("CS11","Lynden Vincent" ));
		Student.REGISTERED_STUDENTSList3.add(new Student( "CS12","Aedan Henson"));
		Student.REGISTERED_STUDENTSList3.add(new Student("CS13","Dhruv Dodson" ));
		Student.REGISTERED_STUDENTSList3.add(new Student("CS14","Alexie Sharpe" ));
		Student.REGISTERED_STUDENTSList3.add(new Student( "CS15","Asim Wicks"));
		Student.REGISTERED_STUDENTSList3.add(new Student("CS16","Vijay Rowland" ));
		Student.REGISTERED_STUDENTSList3.add(new Student( "CS17","Rajveer Davenport"));
		Student.REGISTERED_STUDENTSList3.add(new Student("CS18","Nicholas Chan"));
		Student.REGISTERED_STUDENTSList3.add(new Student( "CS19","Amit Huber"));
	
	
						
		for(Student student2:Student.REGISTERED_STUDENTSList3) {
		
			for(int j=0;j<20;j++) {
				student2.addAttendedLessonsId3(getRandomNumber3(0,attantedLessons3.size()-1));
			}
			
		}
		
		
		
	
	}
	
	public void generateAttantedLessonsList() {
		List<GroupExercise3> exerciseList2=new ArrayList<GroupExercise3>();
		exerciseList2.add(new GroupExercise3("Aquacise", 150));
		exerciseList2.add(new GroupExercise3("Body Blitz", 50));
		exerciseList2.add(new GroupExercise3("Box Fit", 170));
		exerciseList2.add(new GroupExercise3("Yoga", 180));
		Date previousStartDate=new Date();
		try {
			 previousStartDate=new SimpleDateFormat("dd-MM-yyyy").parse("10-03-2022");

		} catch (Exception e) {
			e.printStackTrace();
		}
		List<String> previousDateList3=generatePreviousDates(previousStartDate);
		
		for (String date3 : previousDateList3) {
			
			attantedLessons3.add(
					new Lesson(exerciseList2.get(getIndex()), "morning", date3, 0));
			attantedLessons3.add(
					new Lesson(exerciseList2.get(getIndex()), "morning",  date3, 0));
			attantedLessons3.add(
					new Lesson(exerciseList2.get(getIndex()), "afternoon",  date3, 0));
			attantedLessons3.add(
					new Lesson(exerciseList2.get(getIndex()), "afternoon",  date3, 0));
			attantedLessons3.add(
					new Lesson(exerciseList2.get(getIndex()), "evening",  date3, 0));
			attantedLessons3.add(
					new Lesson(exerciseList2.get(getIndex()), "evening",  date3, 0));
		}

		for(Lesson lesson3:attantedLessons3) {
			int stdAttend3=getRandomNumber3(3, 4);
			lesson3.setStdAttend3(stdAttend3);
			
			for(int j=0;j<stdAttend3;j++) {
				lesson3.setTotalRating3(getRandomNumber3(2, 5));
			}
			
		}
		
	}
	
	public void generateUpcommingLessonsList() {
		
		
		List<GroupExercise3> exerciseList3=new ArrayList<GroupExercise3>();
		exerciseList3.add(new GroupExercise3("Aquacise", 200));
		exerciseList3.add(new GroupExercise3("Body Blitz", 150));
		exerciseList3.add(new GroupExercise3("Box Fit", 100));
		exerciseList3.add(new GroupExercise3("Yoga", 80));
		Date upcommingStartDate3=new Date();
		try {
			 upcommingStartDate3=new SimpleDateFormat("dd-MM-yyyy").parse("01-04-2022");

		} catch (Exception e) {
			e.printStackTrace();
		}
		List<String> upcommingDateList3=generateUpcommingDates(upcommingStartDate3);
		
		for (String date3 : upcommingDateList3) {
			upcommingLessons3.add(
					new Lesson(exerciseList3.get(getIndex()), "morning",  date3, 0));
			upcommingLessons3.add(
					new Lesson(exerciseList3.get(getIndex()), "morning",  date3, 0));
			upcommingLessons3.add(
					new Lesson(exerciseList3.get(getIndex()), "afternoon",  date3, 0));
			upcommingLessons3.add(
					new Lesson(exerciseList3.get(getIndex()), "afternoon",  date3, 0));
			upcommingLessons3.add(
					new Lesson(exerciseList3.get(getIndex()), "evening",  date3, 0));
			upcommingLessons3.add(
					new Lesson(exerciseList3.get(getIndex()), "evening", date3, 0));
		}
		
	
	}
	

	public void generateData() {
		try {
			
			generateAttantedLessonsList();
			generateUpcommingLessonsList();
			generateStudentsList();
			
			
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
}
