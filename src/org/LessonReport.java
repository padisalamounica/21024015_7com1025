package org;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;

import org.data.Lesson;
import org.data.ReportSecond;



public class LessonReport {
	
	
	public void displayReportOne() {

		
		
		try {
			System.out.println(" --------------------------------- Report First -------------------------------------");
			System.out.printf("%-13s | %-25s | %-10s | %-10s | %-5s |%n", "EXERCISE","DATE","TIMMING","STUDENTS","AVG_RATING");
			System.out.println("-----------------------------------------------------------------------------------------------------");
	
			
			for (Lesson lesson2 : DataGenerator.attantedLessons3) {
				System.out.printf("%-13s | %-25s | %-10s | %-10d | %-5.2f |%n",
						lesson2.getExercise3().getexerciseName3(),
						lesson2.getDate3() ,
						lesson2.getTiming3() ,
						lesson2.getStdAttend3(),
						calAvgRating2(lesson2.getTotalRating3(),lesson2.getStdAttend3()));

				
			}

		} catch (Exception ex2) {
			ex2.printStackTrace();
		}

	}
	
	private double calAvgRating2(int totalRating2,int stdAttend2) {
		
		double avgRating2=totalRating2/stdAttend2;
		
		int precision2=2;
	    int scale2 = (int) Math.pow(10, precision2);
	    return (double) Math.round(avgRating2 * scale2) / scale2;
	}
	
public void displayReportTwo() {
	
		System.out.println("--------------------------------- Report Second -----------------------------------");
	
		List<String> exerciseList2=new ArrayList<String>();
		List<ReportSecond> report2List2=new ArrayList<ReportSecond>();
		for(Lesson atdLesson2:DataGenerator.attantedLessons3) {
			String exercise2=atdLesson2.getExercise3().getexerciseName3();
			long price2=atdLesson2.getExercise3().getPrice3();
			if(exerciseList2.contains(exercise2)) {
				for(ReportSecond report2:report2List2) {
					if(report2.getExerciseName3().equalsIgnoreCase(exercise2)) {
						report2.setLessons3(1);
						report2.setTotalIncome3(price2);
						break;
					}
				}
			}
			else {
				exerciseList2.add(exercise2);
				report2List2.add(new ReportSecond(exercise2, 1, price2));
			}
		}
		String exercice2[]=calHighestIncome2(report2List2);
		System.out.println("Exercise with highest income is : "
							+exercice2[1]+"  Total income : "+exercice2[0]);
		
		
		try {
			System.out.printf("%-13s | %-5s %n", "EXERCISE","No of Lesson");
			System.out.println("------------------------------------------");
	
			//System.out.println(" EXERCISE  No of Lesson ");
			for (ReportSecond reportTwo : report2List2) {
				System.out.printf("%-13s | %-5d %n", reportTwo.getExerciseName3(),reportTwo.getLessons3());
				
				
			}


		} catch (Exception ex2) {
			ex2.printStackTrace();
		}

	}

private String[] calHighestIncome2(List<ReportSecond> report2List2) {
	double maxPrice2=0;
	String exerciseName2="";
	
	for (ReportSecond reportTwo : report2List2) {
		double income2=reportTwo.getTotalIncome3();
		if(maxPrice2<income2) {
			maxPrice2=income2;
			exerciseName2=reportTwo.getExerciseName3();
		}
		
	}
	String strMaxPrice2=maxPrice2+"";
	String exercise2[]={strMaxPrice2,exerciseName2};;
	return exercise2;
}
	
}
